# Cookbook Name:: cookbook-contributors-gitlab-com
# Recipe:: default
# License:: MIT
#
# Copyright 2016, GitLab Inc.

apt_update

include_recipe 'cookbook-contributors-gitlab-com::user'
include_recipe 'cookbook-contributors-gitlab-com::ruby'
include_recipe 'cookbook-contributors-gitlab-com::setup'
include_recipe 'cookbook-contributors-gitlab-com::nginx'
