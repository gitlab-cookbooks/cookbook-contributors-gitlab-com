# Cookbook Name:: cookbook-contributors-gitlab-com
# Recipe:: database
# License:: MIT
#
# Copyright 2018, GitLab Inc.
#

database_name = contributors_config['database_name']
database_user = contributors_config['database_user']
database_password = contributors_config['database_password']

return unless contributors_config['database_url'].nil?

postgresql_server_install do
  name database_name
  action %I(install create)
end

postgresql_user database_user do
  password database_password
end

postgresql_database database_name do
  encoding 'UTF-8'
  action :create
end

postgresql_access 'local_postgres_user' do
  access_type 'local'
  access_db database_name
  access_user database_user
  access_method 'md5'
  action :grant
end
