# Cookbook Name:: cookbook-contributors-gitlab-com
# Recipe:: user
# License:: MIT
#
# Copyright 2018, GitLab Inc.
#

username = node['cookbook-contributors-gitlab-com']['username']

user username do
  shell '/bin/false'
  system true
  manage_home true
end

directory "/home/#{username}" do
  recursive true
  owner username
  group username
end
