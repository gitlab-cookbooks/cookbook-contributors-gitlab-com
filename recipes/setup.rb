# Cookbook Name:: cookbook-contributors-gitlab-com
# Recipe:: setup
# License:: MIT
#
# Copyright 2018, GitLab Inc.
#

# native dependencies
%w(
  cmake pkg-config nodejs libpq-dev
).each do |pkg|
  package pkg
end

contributors_config_hash = contributors_config.to_hash
username = contributors_config['username']
app_dir = "/opt/#{contributors_config['app_name']}"
app_ssh_repo = contributors_config['app_ssh_repo']
app_https_repo = contributors_config['app_https_repo']
app_revision = contributors_config['app_revision']
app_rails_env = contributors_config['app_rails_env']
deploy_key = contributors_config['deploy_key']
database_url = contributors_config['database_url']
database_name = contributors_config['database_name']
database_user = contributors_config['database_user']
database_password = contributors_config['database_password']

application app_dir do
  owner username
  group username

  git_repo = deploy_key && app_ssh_repo || app_https_repo
  git git_repo do # ~FC009
    revision app_revision
    deploy_key deploy_key
  end

  bundle_install do
    user username
    deployment true
    without %w(development test)
  end

  template "#{name}/config/secrets.yml" do
    source 'secrets.yml.erb'
    owner username
    group username
    mode '0644'
    variables(contributors_config_hash)
  end

  rails do
    rails_env app_rails_env
    app_database_url = database_url || "postgresql://127.0.0.1/#{database_name}"
    database app_database_url do
      username database_user
      password database_password
      encoding 'unicode'
    end
    migrate true
  end

  puma do
    port 8080
  end
end

app_contrib_repo = node['cookbook-contributors-gitlab-com']['app_contrib_repo']
contrib_repo_name = File.basename(app_contrib_repo)

execute 'setup_git_mirror' do
  user username
  group username
  cwd app_dir
  command ['git', 'clone', '--mirror', app_contrib_repo]
  creates contrib_repo_name
end

file "#{app_dir}/database_git_sync.sh" do
  owner username
  group username
  mode '0755'
  content <<~SYNC_SCRIPT
    #!/bin/bash

    git -C "#{app_dir}/#{contrib_repo_name}" remote update && \
    #{app_dir}/bin/rails runner -e #{app_rails_env} "Repo.sync(path: '#{app_dir}/#{contrib_repo_name}')"
  SYNC_SCRIPT
end

cron 'database_git_sync' do
  user username
  command "/bin/bash #{app_dir}/database_git_sync.sh &>> #{app_dir}/log/sync.log"
  minute '0'
end
