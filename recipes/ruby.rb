# Cookbook Name:: cookbook-contributors-gitlab-com
# Recipe:: ruby
# License:: MIT
#
# Copyright 2018, GitLab Inc.
#

package 'git' # Needed for everything

# Packages for compiling Ruby
%w(
  autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev
  zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev
).each do |pkg|
  package pkg
end

ruby_build_install_dir = '/usr/local/ruby-build'

git ruby_build_install_dir do
  repository 'https://github.com/sstephenson/ruby-build.git'
  notifies :run, 'execute[install ruby-build]', :immediately
end

execute 'install ruby-build' do
  command './install.sh'
  cwd ruby_build_install_dir
  env 'PREFIX' => '/usr/local'
  creates '/usr/local/bin/ruby-build'
end

ruby_version = node['cookbook-contributors-gitlab-com']['ruby_version']
execute "/usr/local/bin/ruby-build #{ruby_version} /usr/local" do
  environment 'RUBY_CONFIGURE_OPTS': '--disable-install-doc'
  not_if "/usr/local/bin/ruby --version | grep 'ruby #{ruby_version}'"
end

rubygems_version = node['cookbook-contributors-gitlab-com']['rubygems_version']
execute "/usr/local/bin/gem update --system #{rubygems_version} --no-ri --no-rdoc" do
  not_if "/usr/local/bin/gem --version | grep '^#{rubygems_version}$'"
end

bundler_version = node['cookbook-contributors-gitlab-com']['bundler_version']
execute "/usr/local/bin/gem install bundler --version #{bundler_version} --no-ri --no-rdoc" do
  not_if "/usr/local/bin/bundle --version | grep ' #{bundler_version}$'"
end
