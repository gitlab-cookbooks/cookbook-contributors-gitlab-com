# Cookbook Name:: cookbook-contributors-gitlab-com
# Recipe:: nginx
# License:: MIT
#
# Copyright 2018, GitLab Inc.
#

app_name = contributors_config['app_name']
ssl_certificate = contributors_config['ssl_certificate']
ssl_key = contributors_config['ssl_key']

package 'nginx' do
  action :install
end

template "/etc/nginx/sites-available/#{app_name}" do
  source 'nginx.erb'
  mode '0644'
end

link "/etc/nginx/sites-enabled/#{app_name}" do
  to "/etc/nginx/sites-available/#{app_name}"
end

file '/etc/nginx/sites-enabled/default' do
  action :delete
end

directory '/etc/nginx/ssl'

file "/etc/nginx/ssl/#{app_name}.crt" do
  mode '0640'
  content ssl_certificate
end

file "/etc/nginx/ssl/#{app_name}.key" do
  mode '0640'
  content ssl_key
end

service 'nginx' do
  action :enable
end

service 'nginx' do
  action :restart
end
