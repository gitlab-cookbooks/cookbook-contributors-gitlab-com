# User
default['cookbook-contributors-gitlab-com']['username'] = 'gitlab-contributors'

# Ruby
default['cookbook-contributors-gitlab-com']['ruby_version'] = '2.4.1'
default['cookbook-contributors-gitlab-com']['rubygems_version'] = '2.7.7'
default['cookbook-contributors-gitlab-com']['bundler_version'] = '1.16.2'

# Postgres - secrets when using an external database
default['cookbook-contributors-gitlab-com']['database_url'] = nil
default['cookbook-contributors-gitlab-com']['database_name'] = 'contributors_gitlab_com_production'
default['cookbook-contributors-gitlab-com']['database_user'] = 'gitlab_contributors'
default['cookbook-contributors-gitlab-com']['database_password'] = 'password'

# Application
default['cookbook-contributors-gitlab-com']['app_name'] = 'contributors-gitlab-com'
default['cookbook-contributors-gitlab-com']['app_revision'] = 'master'
default['cookbook-contributors-gitlab-com']['app_rails_env'] = 'production'
default['cookbook-contributors-gitlab-com']['app_ssh_repo'] = 'git@gitlab.com:gitlab-com/gitlab-contributors.git'
default['cookbook-contributors-gitlab-com']['app_https_repo'] = 'https://gitlab.com/gitlab-com/gitlab-contributors.git'
default['cookbook-contributors-gitlab-com']['app_contrib_repo'] = 'https://gitlab.com/gitlab-org/gitlab-ce.git'
default['cookbook-contributors-gitlab-com']['deploy_key'] = nil # secret
default['cookbook-contributors-gitlab-com']['secret_token'] = '684da12205f96e3d498021739802312addb599c0590295179fb43d7203fb7af7b3cdd4b977a888fe4fded2932e3dd5b7bf8579841a9bb87217efb980ae7f1068' # secret

# nginx - Sample certificate - secrets
default['cookbook-contributors-gitlab-com']['ssl_certificate'] = <<-SSL_CERT
-----BEGIN CERTIFICATE-----
MIIDFzCCAf+gAwIBAgIJAOokitEreaOrMA0GCSqGSIb3DQEBBQUAMCIxIDAeBgNV
BAMMF2NvbnRyaWJ1dG9ycy5naXRsYWIuY29tMB4XDTE4MDgxNjAwMDkyN1oXDTI4
MDgxMzAwMDkyN1owIjEgMB4GA1UEAwwXY29udHJpYnV0b3JzLmdpdGxhYi5jb20w
ggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDAWE4KoiMHARnUCZf2XKkN
frjIycHtjcjD4Qp/8H5To3+Vm8MsneahUF6ofEOkQIVTo1fkbuydi5KGpYjmQDm8
HIMTkap/Pi+sJQ+gx4MarlkkGYuV+NEoa5sEjg9ZSE7AOG1NURLaxnrj/bSbrXVf
xb3FJNEY1rFtlG3XuVZfKSO6GA26QJizTDO/u1glXMrU5KVgwTBYf7MOAq5BReDe
Jlutbi4SZ7bMH+wjcum0syyN4hXRNA5Yqj9UnvmkuNelL5maf3A2LJ5NMfE1Qv9k
OjFhsH0kOksskMmCly/5WQ9hrW19zrnZn+16dL9zN/OHJytJUyqIoANtWR6hlyRB
AgMBAAGjUDBOMB0GA1UdDgQWBBSNuBHvM6JMwPPkkqWmsEccpQBESTAfBgNVHSME
GDAWgBSNuBHvM6JMwPPkkqWmsEccpQBESTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3
DQEBBQUAA4IBAQC9oTUFbCv/VVezSovxNBnIkfAGxSkqWshCAxsXWa9AdsqcK1zC
H9qQwEwze1hyvqGz694U1oYZ3gU7OHXx2lcIdETiO8WZKrI5VSY7Ci2zYcXHD00C
B+u2r9PUpoKs3rhrYqvq0AiurAJOFMeB0car5ZNPT9wakyAHetKNxeGilmnxTz0l
E448QBB4v7rOn9mmRkRHmS2VQI1gVXD3w4x8bfLWsP9wmU+r4wlCEwyQzRB7n1sL
73BmLmlxY6I/dfd5rsOFv8G8qY0+EokJ3hKxg1XcUJy9HdpzBm82bd73YTQ/KRdM
h7ebpZjj1FFSjCbRc7lVWp5ncv2u5rjlVvzf
-----END CERTIFICATE-----
SSL_CERT
default['cookbook-contributors-gitlab-com']['ssl_key'] = <<-SSL_KEY
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAwFhOCqIjBwEZ1AmX9lypDX64yMnB7Y3Iw+EKf/B+U6N/lZvD
LJ3moVBeqHxDpECFU6NX5G7snYuShqWI5kA5vByDE5Gqfz4vrCUPoMeDGq5ZJBmL
lfjRKGubBI4PWUhOwDhtTVES2sZ64/20m611X8W9xSTRGNaxbZRt17lWXykjuhgN
ukCYs0wzv7tYJVzK1OSlYMEwWH+zDgKuQUXg3iZbrW4uEme2zB/sI3LptLMsjeIV
0TQOWKo/VJ75pLjXpS+Zmn9wNiyeTTHxNUL/ZDoxYbB9JDpLLJDJgpcv+VkPYa1t
fc652Z/tenS/czfzhycrSVMqiKADbVkeoZckQQIDAQABAoIBAGz1WPgzEs6T9BKm
gJwBr45Ce+DaUe4FBHbZ033YOWed1ddlTjHHHO/CvGyLQDIajqxreo2fvJAekZmO
yjdMqKgEKsh6GvBLxOuqbujwPDdwDnUht230bl+MXVs+MjmseVfwXxKOW7Ts/I6M
mLQblcL0HGVCTuTYMdhWXcb3aZMrmnZDXvx0XjiqQuaMTPXnj1iE3kxVwI14hQPM
fFT22rOLnH3AtNd3GE0EcXQtmG1rimV8ZTwSmF+rlKn/kOgcQWgyVFAdR4Zah+vw
JQhMM9QvYsxlLhK+bHMgauNtTm41LPuT+KQWagp4/pJxLwQkzc29OqGU89+PNhF0
YXSWzfECgYEA3lHNwKrWTZk6xK4Ez4Yjtipdys/oXUhM5CRxMnSxGLQRkF3xinf5
rhPACGjWCyqkS0GzApSqkWEqroJe0IoNdm4aSdE9A9uCfa1qiJVn87K9fOV/ZHr6
eim7D54EuLzsjFb5R0I6mtxx+GwiKQ8k5aqxVoF4Fj2Vy5bGDbI8piUCgYEA3Xv/
w7bjAqf7zGOTk4XEg/CzuY/cTPZqTQYzzj87wTtFFX98ZojP22M6q68XABmlWcjd
deHMed5rUyLNv8FNElcEyUbvXG9INu9lNVTtGrykjEyHhdjRmjUwB/dXNnDzVzcF
roKxWcgm5VS2/dDsTtlCrwyUls2UkxAL4Mo5xO0CgYBtz7Q0jW2rCJu5mEbyBkzP
Zt/LFKSups6sBcAP7wmmrRLb3zs2bQpbzI+W18mZ3GcocPwtsycJBhih9+1DTl1V
+Q5tFbfwltU+pJCxhDLDca8c6PLiu8fK3Z/zF9c3NMIWk8MvSmxOH8L/tNGuvDc6
ZfSQSQT631KFLCUJkNdaLQKBgQCOZmMS2ksqMgbaaks/1gl7Kw7ih3QEp4nA6/Py
7wTkP88y+zxoUB4T18CpEXGgRtzg1RxN5lcpI6+tS/stBoMe/4O8RTcwHvw3FcPk
fi3TuuWwrqOTkstwcjHnbUdNYWGS+XVVtgGA0Abx+32Lwv+r0lPlaBlAu/yCcRax
+FMcIQKBgFo/edCHUkxE79gCi9E4gIqpyfD92OvzldO6szESPFWVZ4koZoS2Q841
Ef1/k6ECDTniA9DRqDzzwUKoxHuyVITayeE4yzms30clFsfJz8+a1rGYo654HZuE
PNNPJPvWMTZQoSFg7JGZKq4WaZ+gqSPFmuXSvgWbQ+eYJQ0v6iZC
-----END RSA PRIVATE KEY-----
SSL_KEY
