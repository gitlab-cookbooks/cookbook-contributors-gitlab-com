name             'cookbook-contributors-gitlab-com'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Installs/Configures contributors.gitlab.com'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.2.0'
chef_version     '>= 12.1' if respond_to?(:chef_version)
issues_url       'https://gitlab.com/gitlab-cookbooks/cookbook-contributors-gitlab-com/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/cookbook-contributors-gitlab-com'

supports 'ubuntu', '= 16.04'

# Please specify dependencies with version pin:
# depends 'cookbookname', '~> 1.0.0'
depends 'application_git', '~> 1.2.0'
depends 'application_ruby', '~> 4.1.0'
depends 'gitlab-vault', '~> 0.2.0'
depends 'gitlab_secrets', '~> 0.0.6'
