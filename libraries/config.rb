module Gitlab
  module ContributorsCookbook
    def contributors_config
      if (secrets_hash = node['cookbook-contributors-gitlab-com']['secrets']) && !secrets_hash.empty?
        node_secrets = get_secrets(secrets_hash['backend'], secrets_hash['path'], secrets_hash['key'])
        Chef::Mixin::DeepMerge.deep_merge(
          node_secrets['cookbook-contributors-gitlab-com'], node['cookbook-contributors-gitlab-com'].to_hash
        )
      else
        include_recipe 'gitlab-vault'
        GitLab::Vault.get(node, 'cookbook-contributors-gitlab-com')
      end
    end
  end
end

Chef::Recipe.send(:include, Gitlab::ContributorsCookbook)
